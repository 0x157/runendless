import subprocess
import sys

def getCmdlineArgs():
    remainingOptions = ['--help', '--quit_on_graceful_close', '--verbose']
    args = sys.argv[1:]
    showHelp = False
    quitOnGracefulClose = False
    verbose = False

    i = 0
    while i < len(args):
        arg = args[i]
        if arg in remainingOptions:
            remainingOptions.remove(arg)
            if arg == '--help':
                showHelp = True
            elif arg == '--quit_on_graceful_close':
                quitOnGracefulClose = True
            elif arg == '--verbose':
                verbose = True
            i += 1
        else:
            break

    result = {
            'help' : showHelp,
            'quitOnGracefulClose': quitOnGracefulClose,
            'verbose': verbose,
            'cmd': args[i:]
            }
    return result

def printHelp(no_command_provided):
    helpMsg = """Restart a CMD as soon as it finishes.

For example this can be used to automatically restart a crashing application.

Usage: runendless.py [OPTIONS] CMD

Options:
  --quit_on_graceful_close  Don't restart CMD if it finishes with return code 0.
  --verbose                 Show some runtime information.
  --help                    Show this message and exit."""

    if no_command_provided:
        helpMsg = "Missing command!\n\n{}".format(helpMsg)

    print(helpMsg)

def main():

    args = getCmdlineArgs()
    cmd = args['cmd']

    if args['help'] or len(cmd) == 0:
        printHelp(not args['help'] and len(cmd) == 0)
        return

    verbose = args['verbose']
    if verbose:
        print("cmd: {}".format(" ".join(cmd)) )

    while True:
        r = subprocess.run(cmd)
        rc = r.returncode

        if verbose:
            print("rc: {}".format(rc))

        if args['quitOnGracefulClose'] and rc == 0:
            break

if __name__ == '__main__':
    main()
